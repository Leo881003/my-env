# Cheatsheet for Vim
vi, vim 是Linux, Unix 的文字編輯器，以下是常用的指令:

## 編輯模式

- i                 在游標位置進入編輯模式
- I                 在游標行的第一個非空白字元進入編輯模式
- a                 在游標位置後進入編輯模式
- A                 在游標行的最後一個字元進入編輯模式
- o                 向下新增一行，並進入編輯模式
- O                 向上新增一行，並進入編輯模式
- cc                刪除游標行，並進入編輯模式
- [ESC]             取消指令或退出編輯模式


## 游標移動

- gg                移到第一行
- G                 移到最後一行
- <$n> → G          移動到第 n 行
- 0                 移動到該行最前面
- $                 移動到該行最後面
- <$n> → [Space]    向右移動 n 個字元
- <$n> → [Enter]    向下移動 n 行


## 標記與複製

- v                 開始字串標記
- V                 開始行標記
- [Ctrl]-V          開始區塊標記
- d                 刪除標記的內容
- y                 複製標記的內容
- yy                複製游標行
- yG                複製游標行到最後一行
- y1G               複製游標行到第一行
- y$                複製游標處到最後一個字元
- y0                複製游標處到第一個字元
- p                 在下一行貼上複製或刪除的內容
- P                 在上一行貼上複製或刪除的內容
- [Ctrl]-R → 0      在下一行貼上複製或刪除的內容，適用於編輯模式及指令行


## 搜尋與取代

- /搜尋字串         向下搜尋字串
- /\c搜尋字串       向下搜尋字串，不分大小寫
- \*                 將游標移到字串上，直接按 “*" 也可以做向下搜尋
- ?搜尋字串         向上搜尋字串
- ?\c搜尋字串       向上搜尋字串，不分大小寫
- :set ic           搜尋時不分大小寫
- :set noic         搜尋時要分大小寫
- n                 繼續下一個搜尋結果
- N                 繼續上一個搜尋結果
- :起始行,終止行s/搜尋字串/取代字串/gic   從第 n 行到第 n 行取代字串 (後面的 g: 整行全部 i: 不分大小寫 c: 詢問)
- :1,$s/搜尋字串/取代字串/gic 全部取代字串 (後面的 g: 整行全部 i: 不分大小寫 c: 詢問)


## 刪除

- dd                刪除游標行
- <$n> → dd         刪除 n 行
- dG                刪除游標行到最後一行
- d1G               刪除游標行到第一行
- d$                刪除游標處到最後一個字元
- d0                刪除游標處到第一個字元


## 檔案功能

- :w                存檔 (加 ! 表示強制存檔)
- :w <filename>     另存新檔
- :q                退出 (加 ! 表示不存檔強制退出)
- :wq               存檔並退出
- :x                存檔並退出
- :w !sudo tee %    當你編輯好檔案要存檔時，卻發現沒有寫入檔案的權限! 用這會指令可以讓你直接以 root 的權限存檔
- :e <filename>     編輯其它檔案
- :e!               還原至檔案編修前的狀態
- :r <filename>     讀入檔案內容，並加到游標行的後面
- :n                切換到下一個開啟的檔案
- :N                切換到上一個開啟的檔案
- :set nu           顯示行號
- :set nonu         取消行號顯示
- :files            列出所有開啟的檔案
- :Ex               開啟檔案瀏覽器
- :Ex <path>        於指定路徑開啟檔案瀏覽器
- :Hex              分割水平視窗後，再開啟檔案瀏覽器
- :Vex              分割垂直視窗後，再開啟檔案瀏覽器
- :Tex              新增頁籤後，再開啟檔案瀏覽器
- :Hex <path>       分割水平視窗後，再於指定路徑開啟檔案瀏覽器
- :Vex <path>       分割垂直視窗後，再於指定路徑開啟檔案瀏覽器
- :Tex <path>       新增頁籤後，再於指定路徑開啟檔案瀏覽器


## 視窗分割

- :new              新增水平分割視窗
- :new <filename>   新增水平分割視窗，並在新增的視窗載入檔案
- :vnew             新增垂直分割視窗
- :vnew <filename>  新增垂直分割視窗，並在新增的視窗載入檔案
- :sp               新增水平分割視窗，並在新增的視窗載入目前的檔案
- :sp <filename>    新增水平分割視窗，並在新增的視窗載入檔案
- :vsp              新增垂直分割視窗，並在新增的視窗載入目前的檔案
- :vsp <filename>   新增垂直分割視窗，並在新增的視窗載入檔案
- [Ctrl]-W → [方向鍵] 切換視窗
- :only             僅保留目前的視窗


## 頁籤

- :tabe             新增頁籤
- :tabe <filename>  新增頁籤，並在新頁籤載入檔案
- :tabc             關閉目前的頁籤，等同 :q
- :tabo             關閉所有頁籤
- :tabn             移至下一個頁籤
- :tabp             移至上一個頁籤
- <$n>gt            移至第n個頁籤
- gt                移至下一個頁籤
- gT                移至上一個頁籤
- :tabfirst         移至第一個頁籤
- :tablast          移至最後一個頁籤
- :tabm <$n>        移至特定編號的頁籤 (編號從 0 開始)
- :tabs             列出所有頁籤


## 其它指令

- J                 將游標行與下一行合併
- u                 還原指令
- [Ctrl]-R          重做指令
- [Ctrl]-N          自動補齊曾輸入過的單字
- .                 重覆上一個指令
- !<cmd>            執行 linux 指令，並顯示執行結果
- :TOhtml           將目前編輯的檔案轉換成 HTML 原始碼 (會新增一個水平分割視窗)


## 檔案瀏覽器操作 (Vim native)

*請先以 :Ex 相關指令進入檔案瀏覽器*

- \-                到上層目錄
- d                 建立目錄
- D                 刪除目錄
- R                 重新命名
- s                 切換排序方式
- r                 切換升冪/降冪排序
- i                 切換檔案的排列方式
- /                 搜尋字串
- x                 執行檔案
- o                 新增水平視窗
- v                 新增垂直視窗

