[Appearance]
ColorScheme=DarkPastels
Font=Noto Mono,12,-1,5,50,0,0,0,0,0

[General]
Name=Bash
Parent=FALLBACK/
TerminalColumns=150
TerminalRows=50

[Terminal Features]
UrlHintsModifiers=67108864
