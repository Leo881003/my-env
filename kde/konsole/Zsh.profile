[Appearance]
ColorScheme=DarkPastels
Font=DejaVu Sans Mono,14,-1,5,50,0,0,0,0,0,Book
UseFontLineChararacters=true

[General]
Command=/bin/zsh
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=Zsh
Parent=FALLBACK/
StartInCurrentSessionDir=true
TerminalColumns=150
TerminalRows=50

[Interaction Options]
TrimTrailingSpacesInSelectedText=true

[Keyboard]
KeyBindings=xterm

[Scrolling]
HistoryMode=2

[Terminal Features]
UrlHintsModifiers=67108864
