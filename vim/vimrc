call plug#begin('~/.vim/plugged')
let $PLUGLOCAL = $HOME . "/.vim/plug.local.vim"
if filereadable($PLUGLOCAL)
    source $PLUGLOCAL
endif
" Asynchronous Lint Engine
Plug 'dense-analysis/ale'
" TOML syntax
Plug 'cespare/vim-toml'
" A bar showing ctags
Plug 'majutsushi/tagbar'
" clang-format support
Plug 'rhysd/vim-clang-format', { 'on': ['ClangFormat'] }
" Fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'
" Multiple Cursor support
Plug 'mg979/vim-visual-multi'
" Useful Utility Commands
Plug 'tpope/vim-eunuch'
" File browser
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Support .editerconfig
Plug 'editorconfig/editorconfig-vim'
" HTML Tags Input Helper
Plug 'mattn/emmet-vim', { 'for': ['htm', 'html'] }
" Normal Mode Parentheses Add/Modify/Delete
Plug 'tpope/vim-surround'
" Auto Pair Parentheses
Plug 'jiangmiao/auto-pairs'
" Better '*' searching
Plug 'bronson/vim-visual-star-search'
" Switching between single-line and multi-line
Plug 'AndrewRadev/splitjoin.vim'
" Automatically relative line number
Plug 'jeffkreeftmeijer/vim-numbertoggle'
" Git Commands
Plug 'tpope/vim-fugitive'
" Git diff showing
Plug 'airblade/vim-gitgutter'
" Scrolling Animating
Plug 'yuttie/comfortable-motion.vim'
" Airline Appearance
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Tender theme
Plug 'jacoborus/tender.vim'
" Better tmux integration
Plug 'tmux-plugins/vim-tmux-focus-events'
" My custom plugin
Plug '~/.vimrc-plug'
call plug#end()

syntax on
filetype plugin indent on
set number
set t_Co=256
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
set cursorline
set noerrorbells
set background=dark
set enc=utf8
set mouse=a
set ignorecase
set hlsearch
set history=50
set autoindent
set laststatus=2
set showtabline=2
set updatetime=1000
set pastetoggle=<F10>
set clipboard=unnamedplus

nmap <F7> :TagbarToggle<CR>
nmap <F8> :NERDTreeToggle<CR>
nmap <F9> :GitGutterToggle<CR>

let mapleader = ' '
nnoremap <silent> <Leader>w :w<CR>
nnoremap <silent> <Leader>W :w!<CR>
nnoremap <silent> <Leader>x :wq<CR>
nnoremap <silent> <Leader>q :q<CR>
nnoremap <silent> <Leader>p :CtrlPTag<CR>
nnoremap <silent> <Leader>f :ALEFix<CR>

if has("autocmd")
    " have Vim jump to the last position when reopening a file
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

    " Exit Vim if NERDTree is the only window left.
    autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
                \ quit | endif
    " Open the existing NERDTree on each new tab.
    autocmd BufWinEnter * silent NERDTreeMirror

    augroup _my_filetypes
        autocmd!
        " Hot key for clangformat
        autocmd FileType c,cpp,objc nnoremap <silent> <Leader>f :ClangFormat<CR>

        " List of special indent settings
        autocmd FileType fstab setlocal shiftwidth=8 tabstop=8 noexpandtab
        autocmd FileType make setlocal noexpandtab
        autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 expandtab
    augroup END
endif

" Add compatibility for colorscheme
if has("termguicolors")
    set t_8f=[38;2;%lu;%lu;%lum
    set t_8b=[48;2;%lu;%lu;%lum
    set termguicolors
endif

colorscheme tender

let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_warning_symbol = '->'

" ALE configurations
let g:ale_sign_column_always = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '->'
let g:ale_fixers = {'*': ['remove_trailing_lines', 'trim_whitespace']}
let g:ale_c_clangformat_use_local_file = 1

let g:gitgutter_max_signs = 1000

"let g:AutoPairsFlyMode = 1
let g:AutoPairsMapBS = 0

let g:tagbar_autofocus = 1
let g:tagbar_autoshowtag = 1

" Airline configurations
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ycm#enabled = 1
let g:airline_theme = 'tender'

let g:clang_format#detect_style_file = 1
let g:clang_format#auto_format = 0
let g:clang_format#auto_format_on_insert_leave = 0

command Cpp11 execute "!g++ -lm -lcrypt -O2 -std=c++11 -o " . expand('%:r') . " " . @%
command Cansi execute "!gcc -lm -lcrypt -O2 -ansi -o " . expand('%:r') . " " . @%
command Reload execute ":source $MYVIMRC"

