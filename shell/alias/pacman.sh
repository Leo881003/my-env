if has_command pacman; then
    alias sps='sudo pacman -S'
    alias spss='pacman -Ss'
    alias spsi='pacman -Si'
    alias spsy='sudo pacman -Sy'
    alias spsyu='sudo pacman -Syu'
    alias pacql='pacman -Ql'
    alias pacqi='pacman -Qi'
fi
